import React from 'react';
import { RxCross1 } from 'react-icons/rx';

const Cart = ({ setOpenCart }) => {
    return (
        <div className="fixed top-0 left-0 w-full bg-[#0000004b] h-screen z-10">
            <div className="fixed top-0 right-0 h-full w-[80%] 800px:w-[25%] bg-white flex flex-col overflow-y-scroll justify-between shadow-sm">
                <div className="w-full h-screen flex items-center justify-center">
                    <div className="flex w-full justify-end pt-5 pr-5 fixed top-3 right-3">
                        <RxCross1 size={25} className="cursor-pointer" onClick={() => setOpenCart(false)} />
                    </div>
                    <h5>Cart Items is empty!</h5>
                </div>
            </div>
        </div>
    );
};

export default Cart;
