import ForgotPassword from '../components/Auth/ForgotPassword';
import { Footer, Header } from '../routes/RoutesLayout';

const ForgotPasswordPage = () => {
    return (
        <>
            <Header />
            <ForgotPassword />
            <Footer />
        </>
    );
};

export default ForgotPasswordPage;
