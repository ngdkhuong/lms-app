import { Header } from '../routes/RoutesLayout';
import Register from './../components/Auth/Register';

const RegisterPage = () => {
    return (
        <>
            <Header />
            <Register />
        </>
    );
};

export default RegisterPage;
