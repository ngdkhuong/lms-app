import ResetPassword from '../components/Auth/ResetPassword';
import { Footer, Header } from '../routes/RoutesLayout';

const ResetPasswordPage = () => {
    return (
        <>
            <Header />
            <ResetPassword />
            <Footer />
        </>
    );
};

export default ResetPasswordPage;
