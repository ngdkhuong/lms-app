import Login from '../components/Auth/Login';
import { Footer, Header } from '../routes/RoutesLayout';

const LoginPage = () => {
    return (
        <>
            <Header />
            <Login />
            <Footer />
        </>
    );
};

export default LoginPage;
