import RegisterPage from './../pages/RegisterPage';
import ForgotPasswordPage from './../pages/ForgotPasswordPage';
import ResetPasswordPage from './../pages/ResetPasswordPage';
import LoginPage from '../pages/LoginPage';

export { LoginPage, RegisterPage, ForgotPasswordPage, ResetPasswordPage };
