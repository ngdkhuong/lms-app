import Header from '../components/Layout/Header';
import Footer from '../components/Layout/Footer';
import Slider from '../components/Layout/Slider';

export { Header, Footer, Slider };
